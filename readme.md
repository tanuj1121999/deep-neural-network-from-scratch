This project is used to classify cats and non-cats using a neural network. Forward propagation and back propagation is implemented 
from scratch in python. The neural net consists of 4 layers. Gradient descent is used to optimize the loss and is reduced to 0.092878 
after 2400 iterations.
